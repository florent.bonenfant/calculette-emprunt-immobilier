import React, { createContext, useState } from "react";

const FieldsContext = createContext({} as any);

const FieldsProvider = ({ children }: any) => {
  const [fields, setFields] = useState<any>({});

  return (
    <FieldsContext.Provider value={{ fields, setFields }}>
      {children}
    </FieldsContext.Provider>
  );
};

const WithFields = (Comp: any) => (props: any) => (
  <FieldsContext.Consumer>
    {({ fields, setFields }) => (
      <Comp {...props} fields={fields} setFields={setFields} />
    )}
  </FieldsContext.Consumer>
);

const Consumer = FieldsContext.Consumer;

export {
  WithFields,
  FieldsProvider as Provider,
  FieldsContext as Context,
  Consumer,
};
