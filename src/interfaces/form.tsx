export interface IFormFieldset {
  legend?: string;
  classFieldset?: string;
  children: object;
}

export default interface IFormField {
  name: string;
  children: string;
  bold: boolean;
  suffix?: string;
  fields?: any;
  setFields?: any;
  changeValue?: any;
  placeholder?: string;
  defaultValue?: string;
  classInput?: string;
  titleInput?: string;
  readOnly?: boolean;
}

export interface IFormBlocFields {
  children: object;
}

export interface IPieGraphData {
  name: string;
  y: number;
  sliced?:boolean;
  selected?:boolean;
}