import React, { useState, useEffect } from "react";
import FormField from "../elements/FormField";
import FormBlocFields from "../elements/FormBlocFields";
import FormFieldset from "../elements/FormFieldset";
import { WithFields } from "../../context/Fields";
import { calculCapaciteRemboursement, castFloat } from "../../helpers/calculs";

export const Information: React.FC = ({ fields, setFields }: any) => {
  const [newValues, setNewValues] = useState<any>({});

  useEffect(() => {
    if (fields["revenue"] !== undefined && !isNaN(fields["revenue"])) {
      localStorage.setItem('revenue', fields["revenue"]);
      localStorage.setItem('revenue_impot', fields["revenue_impot"]);
      localStorage.setItem('apport', fields["apport"]);
      localStorage.setItem('remboursement', calculCapaciteRemboursement(fields).toFixed(2));

      setNewValues({
        ...newValues,
        "remboursement": localStorage.getItem('remboursement'),
      });
    } else {
      let initRevenue = castFloat(localStorage.getItem('revenue')),
       initRevenue_impot = castFloat(localStorage.getItem('revenue_impot')),
       initApport = castFloat(localStorage.getItem('apport'));
      if (initRevenue !== 0 || initRevenue_impot !== 0) {
        setFields({
          ...fields,
          "revenue": initRevenue,
          "revenue_impot": initRevenue_impot,
          "apport": initApport,
          "remboursement": localStorage.getItem('remboursement'),
        });
      }
    }
  }, [fields]);

  return (
    <FormFieldset legend="Information de base">
      <FormBlocFields>
        <FormField name="revenue" suffix="€" bold={true}>
          Revenue :
        </FormField>
        <FormField name="apport" suffix="€" bold={true}>
          Apport personnel :
        </FormField>
        <FormField name="revenue_impot" suffix="%" bold={false}>
          Impot :
        </FormField>
      </FormBlocFields>
      <FormBlocFields>
        <FormField name="remboursement" suffix="€" bold={false} changeValue={newValues} readOnly={true}>
          Possibilité théorique d'emprunt :
        </FormField>
      </FormBlocFields>
    </FormFieldset>
  );
};

export default WithFields(Information);
