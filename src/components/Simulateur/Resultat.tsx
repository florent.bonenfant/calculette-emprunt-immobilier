import React, { useState, useEffect } from "react";
import FormField from "../elements/FormField";
import FormBlocFields from "../elements/FormBlocFields";
import FormFieldset from "../elements/FormFieldset";
import { WithFields } from "../../context/Fields";
import { calculCoutCarburantTrajetParMois, coutAchatTotal, coutMensuelComplet, calculCoutAgenceImmo, calculMontantInteretsTotal, calculMontantEmpruntMensuelTotal, castFloat, calculCoutNotaire, montantFacturesPrevisionnelles, calculMontantCreditTotal } from "../../helpers/calculs";
import * as Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { IPieGraphData } from "../../interfaces/form";

export const Resultat: React.FC = ({fields, setFields}:any) => {
  const [newValues, setNewValues] = useState<any>({});
  const [graphDataMensuel, setGraphDataMensuel] = useState<IPieGraphData[]>([]);
  const [graphDataAchat, setGraphDataAchat] = useState<IPieGraphData[]>([]);

  const [colorResultRemboursementCredit, setColorResultRemboursementCredit] = useState<string>('background-default');
  const [colorResultDepensesMensuel, setColorResultDepensesMensuel] = useState<string>('background-default');

  useEffect(() => {
    let result_cout_mensuel:number = castFloat(calculMontantEmpruntMensuelTotal(fields).toFixed(2)),
     result_cout_mensuel_complet:number = castFloat(coutMensuelComplet(fields).toFixed(2)),
     result_credit:number = castFloat(calculMontantInteretsTotal(fields).toFixed(2)),
     result_logement:number = castFloat(castFloat(fields['logement_prix']).toFixed(2)),
     result_cout_agence:number = castFloat(calculCoutAgenceImmo(fields).toFixed(2)),
     result_cout_notaire:number = castFloat(calculCoutNotaire(fields).toFixed(2)),
     result_factures:number = castFloat(montantFacturesPrevisionnelles(fields).toFixed(2)),
     result_trajets:number = castFloat(calculCoutCarburantTrajetParMois(fields).toFixed(2)),
     result_achat_total:number = castFloat(coutAchatTotal(fields).toFixed(2)),
     montantSalaire:number = castFloat(castFloat(fields['revenue']).toFixed(2)),
     logement_electricite:number = castFloat(fields['logement_electricite']),
     logement_eau:number = castFloat(fields['logement_eau']),
     logement_autre:number = castFloat(fields['logement_autre']),
     montantApport:number = castFloat(fields['apport']),
     montantEmpruntTheorique:number = castFloat(fields['remboursement']),
     montantChargeCopro:number = castFloat(fields['logement_charges']) / 12,
     montantTaxeFonciere:number = castFloat(fields['logement_foncier']) / 12,
     montantAssuranceMensuel:number = castFloat(fields["logement_assurance"]) / 12,
     montantCredits:number = castFloat(calculMontantCreditTotal(fields)),
     taux_endettement:number = result_cout_mensuel * 100 / montantSalaire
     ;

     setGraphDataMensuel([
      {
        name: 'Montant trajets',
        y: result_trajets
      },
      {
        name: 'Assurance habitation',
        y: montantAssuranceMensuel
      },
      {
        name: 'Electricité',
        y: logement_electricite
      },
      {
        name: 'Eau',
        y: logement_eau
      },
      {
        name: 'Autres factures',
        y: logement_autre
      },
      {
        name: 'Crédit(s)',
        y: result_cout_mensuel,
        selected: true,
        sliced: true
      },
      {
        name: 'Taxe foncière',
        y: montantTaxeFonciere
      },
      {
        name: 'Charges copropriété',
        y: montantChargeCopro
      },
      {
        name: 'Reste à vivre',
        y: montantSalaire - (montantTaxeFonciere + result_cout_mensuel + montantChargeCopro + result_trajets + montantAssuranceMensuel + logement_electricite + logement_eau + logement_autre)
      },
    ]);
    setGraphDataAchat([
      {
        name: 'Montant des crédits',
        y: montantCredits
      },
      {
        name: 'Intéret des crédits',
        y: result_credit
      },
      {
        name: 'Agence Immobilière',
        y: result_cout_agence
      },
      {
        name: 'Notaire',
        y: result_cout_notaire
      },
      // {
      //   name: 'Apport',
      //   y: result_achat_total - ((montantCredits - result_credit) + result_cout_notaire + result_cout_agence + result_credit)
      // },
      {
        name: 'Apport',
        y: montantApport,
        selected: true,
        sliced: true
      },
    ]);

        setNewValues({
          ...newValues,
          'result_trajets': result_trajets,
          'result_credit': result_credit,
          'result_logement': result_logement,
          'result_cout_mensuel_complet': result_cout_mensuel_complet,
          'result_cout_mensuel': result_cout_mensuel,
          'result_cout_agence': result_cout_agence,
          'result_cout_notaire': result_cout_notaire,
          'result_achat_total': result_achat_total,
          'result_factures': result_factures,
          'tauxEndettement': taux_endettement.toFixed(2) + ' %'
        });

    // Change la couleur pour l'emprunt des banques
    if (montantEmpruntTheorique > result_cout_mensuel) {
      // Le seuil bancaire
      setColorResultRemboursementCredit('background-success');
    } else if ((montantEmpruntTheorique * 0.1 + montantEmpruntTheorique) > result_cout_mensuel) {
      // Je suis sur il y a moyen de gratter au pres des banques
      setColorResultRemboursementCredit('background-warning');
    } else {
      // Bon alors la par contre t'as interet à avoir du charisme !
      setColorResultRemboursementCredit('background-danger');
    }


    // Change la couleur pour le cout de la vie avec le prix de l'appartement et les principales factures
    if (result_cout_mensuel_complet < montantSalaire - (montantSalaire * 0.35 )) {
      // + de 35 % du salaire restant la belle vie ... :)
      setColorResultDepensesMensuel('background-default');
    } else if (result_cout_mensuel_complet < montantSalaire - (montantSalaire * 0.15 )) {
      // 35 % du salaire, ça semble pas mal :o)
      setColorResultDepensesMensuel('background-warning');
    } else {
      // 15 % du salaire ca semble craignos :x)
      setColorResultDepensesMensuel('background-danger');
    }
  }, [fields]);

  return (
    <FormFieldset legend="Résultat">
      <FormBlocFields>
        <FormField name="result_logement" suffix="€" bold={false} changeValue={newValues} readOnly={true}>
        Prix logement :
        </FormField>
        <FormField name="result_achat_total" suffix="€" bold={false} changeValue={newValues} readOnly={true}>
        Cout d'achat total :
        </FormField>
        <FormField name="result_cout_mensuel" suffix="€" bold={false} changeValue={newValues} classInput={colorResultRemboursementCredit} titleInput={newValues.tauxEndettement}>
        Cout mensuel du credit :
        </FormField>
        <FormField name="result_cout_mensuel_complet" suffix="€" bold={false} changeValue={newValues} classInput={colorResultDepensesMensuel}>
        Cout mensuel complet :
        </FormField>
      </FormBlocFields>

      <FormBlocFields>
        <FormField name="result_credit" suffix="€" bold={false} changeValue={newValues} readOnly={true}>
        Interets des crédits :
        </FormField>
        <FormField name="result_cout_agence" suffix="€" bold={false} changeValue={newValues} readOnly={true}>
        Cout de l'agence :
        </FormField>
        <FormField name="result_cout_notaire" suffix="€" bold={false} changeValue={newValues} readOnly={true}>
        Cout du notaire :
        </FormField>
      </FormBlocFields>

      <FormBlocFields>
        <FormField name="result_factures" suffix="€" bold={false} changeValue={newValues} readOnly={true}>
        Factures prévisionnelles :
        </FormField>
        <FormField name="result_trajets" suffix="€" bold={false} changeValue={newValues} readOnly={true}>
        Cout trajets / mois :
        </FormField>
      </FormBlocFields>

      <div style={{ width:'50%', display: "inline-block"}}>
        <HighchartsReact highcharts={Highcharts} options={{
          credits: {
            enabled: false
          },
          accessibility: {
            point: {
                valueSuffix: '€'
            }
          },
          tooltip: {
            pointFormat: '<b>{point.y:.2f} €</b>'
          },
          title: {
            text: "Répartition du cout de l'achat",
          },
          series: [
            {
              type: "pie",
              name: null,
              data: graphDataAchat
            },
          ],
        }} />
        </div>
        <div style={{ width:'50%', display: "inline-block"}}>
        <HighchartsReact highcharts={Highcharts} options={{
          credits: {
            enabled: false
          },
          accessibility: {
            point: {
                valueSuffix: '€'
            }
          },
          tooltip: {
            pointFormat: '<b>{point.y:.2f} €</b>'
          },
          title: {
            text: "Dépenses prévisionnelles",
          },
          series: [
            {
              type: "pie",
              data: graphDataMensuel,
            },
          ],
        }} />
      </div>
    </FormFieldset>
  );
};

export default WithFields(Resultat);
