import React, { useState, useEffect } from "react";
import FormField from "../elements/FormField";
import FormBlocFields from "../elements/FormBlocFields";
import FormFieldset from "../elements/FormFieldset";
import { WithFields } from "../../context/Fields";
import { calculMontantInterets, calculMontantEmpruntMensuel } from "../../helpers/calculs";

export const Credit: React.FC = ({ fields, setFields, idCredit }: any) => {
  const [newValues, setNewValues] = useState<any>({});
  useEffect(() => {
    setNewValues({
      ...newValues,
      ["result_credit_mensuel_" + idCredit]: calculMontantEmpruntMensuel(
        fields,
        idCredit
      ).toFixed(2),
      ["result_credit_interet_" + idCredit]: calculMontantInterets(
        fields,
        idCredit
      ).toFixed(2),
    });
  }, [fields]);

  return (
    <FormFieldset legend={"Crédit n° " + (idCredit + 1)} classFieldset="wrapper-credit">
      <FormBlocFields>
        <FormField
          name={"credit_montant_" + idCredit}
          suffix="€"
          bold={true}
          changeValue={newValues}
          placeholder="40000"
        >
          Montant :
        </FormField>
        <FormField
          name={"credit_taux_" + idCredit}
          suffix="%"
          bold={true}
          changeValue={newValues}
          placeholder="1.6"
        >
          Taux :
        </FormField>
        <FormField
          name={"credit_duree_" + idCredit}
          suffix="ans"
          bold={true}
          changeValue={newValues}
        >
          Durée :
        </FormField>
        <FormField
          name={"credit_assurance_" + idCredit}
          suffix="%"
          bold={false}
          changeValue={newValues}
          placeholder="0.35"
        >
          Taux assurance :
        </FormField>
        <FormField
          name={"result_credit_interet_" + idCredit}
          suffix="€"
          bold={false}
          changeValue={newValues}
          readOnly={true}
        >
          Cout interet :
        </FormField>
        <FormField
          name={"result_credit_mensuel_" + idCredit}
          suffix="€"
          bold={false}
          changeValue={newValues}
          readOnly={true}
        >
          Cout mensuel :
        </FormField>
      </FormBlocFields>
    </FormFieldset>
  );
};

export default WithFields(Credit);