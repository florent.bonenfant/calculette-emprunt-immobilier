import React, { useState, useEffect } from "react";
import FormField from "../elements/FormField";
import FormBlocFields from "../elements/FormBlocFields";
import FormFieldset from "../elements/FormFieldset";
import { WithFields } from "../../context/Fields";
import { castFloat } from "../../helpers/calculs";

export const Logement: React.FC = ({fields, setFields}:any) => {
  const [newValues,] = useState<any>({});

  useEffect(() => {
    if (
      (fields["logement_notaire"] !== undefined && !isNaN(fields["logement_notaire"]))
      || (fields["logement_agence"] !== undefined && !isNaN(fields["logement_agence"]))
      || (fields["logement_assurance"] !== undefined && !isNaN(fields["logement_assurance"]))
      || (fields["logement_electricite"] !== undefined && !isNaN(fields["logement_electricite"]))
      || (fields["logement_eau"] !== undefined && !isNaN(fields["logement_eau"]))
      || (fields["logement_electricite"] !== undefined && !isNaN(fields["logement_electricite"]))
      || (fields["logement_autre"] !== undefined && !isNaN(fields["logement_autre"]))
    ) {
      localStorage.setItem('logement_notaire', fields["logement_notaire"]);
      localStorage.setItem('logement_agence', fields["logement_agence"]);
      localStorage.setItem('logement_assurance', fields["logement_assurance"]);
      localStorage.setItem('logement_electricite', fields["logement_electricite"]);
      localStorage.setItem('logement_eau', fields["logement_eau"]);
      localStorage.setItem('logement_autre', fields["logement_autre"]);
    } else {
      let initNotaire = castFloat(localStorage.getItem('logement_notaire')),
       intiAgence = castFloat(localStorage.getItem('logement_agence')),
       initElectricite = castFloat(localStorage.getItem('logement_electricite')),
       initEau = castFloat(localStorage.getItem('logement_eau')),
       initAutre = castFloat(localStorage.getItem('logement_autre')),
       initAssurance = castFloat(localStorage.getItem('logement_assurance'));
      if (initNotaire !== 0 || intiAgence !== 0 || initAssurance !== 0 || initElectricite !== 0) {
        setFields({
          ...fields,
          "logement_notaire": initNotaire,
          "logement_agence": intiAgence,
          "logement_assurance": initAssurance,
          "logement_electricite": initElectricite,
          "logement_eau": initEau,
          "logement_autre": initAutre,
        });
      }
    }
  }, [fields]);

  return (
    <FormFieldset legend="Logement">
      <FormBlocFields>
        <FormField name="logement_prix" suffix="€" bold={true} changeValue={newValues} placeholder="180000">
          Prix logement :
        </FormField>
        <FormField name="logement_foncier" suffix="€" bold={true} changeValue={newValues}>
          Taxe foncière :
        </FormField>
        <FormField name="logement_charges" suffix="€/an" bold={true} changeValue={newValues}>
          Charges copropriété :
        </FormField>
        <FormField name="logement_notaire" suffix="%" bold={false} changeValue={newValues}>
          Frais de notaire :
        </FormField>
        <FormField name="logement_agence" suffix="%" bold={false} changeValue={newValues}>
          Frais d'agence (1-7%) :
        </FormField>
      </FormBlocFields>
      <FormBlocFields>
        <FormField name="logement_assurance" suffix="€/an" bold={false} changeValue={newValues}>
          Assurance habitation :
        </FormField>
        <FormField name="logement_electricite" suffix="€/mois" bold={false} changeValue={newValues}>
          Electricité :
        </FormField>
        <FormField name="logement_eau" suffix="€/mois" bold={false} changeValue={newValues}>
          Eau :
        </FormField>
        <FormField name="logement_autre" suffix="€/mois" bold={false} changeValue={newValues}>
          Autre :
        </FormField>
      </FormBlocFields>
    </FormFieldset>
  );
};

export default WithFields(Logement);
