import React, { useEffect } from "react";
import FormField from "../elements/FormField";
import FormBlocFields from "../elements/FormBlocFields";
import FormFieldset from "../elements/FormFieldset";
import { WithFields } from "../../context/Fields";
import { castFloat } from "../../helpers/calculs";

export const Trajet: React.FC = ({ fields, setFields }: any) => {
  useEffect(() => {
    if (
      fields["trajet_nb_trajet"] !== undefined &&
      !isNaN(fields["trajet_nb_trajet"])
    ) {
      localStorage.setItem("trajet_nb_trajet", fields["trajet_nb_trajet"]);
      localStorage.setItem("trajet_conso", fields["trajet_conso"]);
      localStorage.setItem("trajet_essence", fields["trajet_essence"]);
    } else {
      let initNbTrajets = castFloat(localStorage.getItem("trajet_nb_trajet")),
        intiConsoTrajet = castFloat(localStorage.getItem("trajet_conso")),
        intiEssence = castFloat(localStorage.getItem("trajet_essence"));
      if (initNbTrajets !== 0 || intiConsoTrajet !== 0 || intiEssence !== 0) {
        setFields({
          ...fields,
          trajet_nb_trajet: initNbTrajets,
          trajet_conso: intiConsoTrajet,
          trajet_essence: intiEssence,
        });
      }
    }
  }, [fields]);

  return (
    <FormFieldset legend="Trajet boulot">
      <FormBlocFields>
        <FormField name="trajet_distance" suffix="km" bold={true}>
          Distance du logement :
        </FormField>
        <FormField name="trajet_duree" suffix="min" bold={false}>
          Durée du trajet :
        </FormField>
      </FormBlocFields>
      <FormBlocFields>
        <FormField name="trajet_nb_trajet" bold={false} placeholder="2">
          Nombre de trajet :
        </FormField>
        <FormField
          name="trajet_conso"
          suffix="l/100"
          bold={false}
          placeholder="4.3"
        >
          Consommation :
        </FormField>
        <FormField
          name="trajet_essence"
          suffix="€/l"
          bold={false}
          placeholder="1.392"
        >
          Essence :
        </FormField>
      </FormBlocFields>
    </FormFieldset>
  );
};

export default WithFields(Trajet);
