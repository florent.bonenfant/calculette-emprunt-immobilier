import React from "react";
import {IFormBlocFields} from "../../interfaces/form"

export const FormBlocFields: React.FC<IFormBlocFields> = ({
  children
}) => {
  return (
    <div className="wrapper-bloc">
        {children}
    </div>
  );
};

export default FormBlocFields;
