import React from "react";
import { WithFields } from "../../context/Fields";
import IFormField from "../../interfaces/form";

export const FormField: React.FC<IFormField> = ({
  name,
  children,
  suffix,
  bold,
  fields,
  setFields,
  changeValue,
  placeholder,
  defaultValue,
  readOnly,
  classInput,
  titleInput,
}) => {
  var inputValue = "";
  if (fields[name] !== undefined) {
    inputValue = fields[name];
  }
  if (changeValue !== undefined && changeValue[name] !== undefined) {
    inputValue = changeValue[name];
    if (String(changeValue[name]) === "NaN") {
      inputValue = "";
    }
  }

  return (
    <div className="wrapper-element">
      <label className={bold ? "bold" : ""} htmlFor={name}>
        {children}
      </label>
      <div className="wrapper-input">
        <input
          type="text"
          name={name}
          id={name}
          placeholder={placeholder ?? ""}
          value={inputValue}
          onChange={(e) =>
            setFields({
              ...fields,
              [e.target.name]: e.target.value,
            })
          }
          defaultValue={defaultValue}
          readOnly={readOnly ?? false}
          className={classInput ?? ''}
          title={titleInput ?? ''}
        />
        {
          (suffix ? <span className="suffix">{suffix}</span> : null)
        }

      </div>
    </div>
  );
};

export default WithFields(FormField);
