import React from "react";
import { IFormFieldset } from "../../interfaces/form";

export const FormFieldset: React.FC<IFormFieldset> = ({
  legend,
  children,
  classFieldset,
}) => {
  return (
    <div className={classFieldset ?? ""}>
      <fieldset>
        <legend>{legend}</legend>
        {children}
      </fieldset>
    </div>
  );
};

export default FormFieldset;
