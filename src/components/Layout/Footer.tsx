import React from 'react';

export const Footer: React.FC = () => {
    return <footer>
        <div>Ce simulateur permet simplement de se faire une idée des couts d'une future acquisition, il ne remplace en rien des professionnels du secteur.</div>
    </footer>
}

export default Footer;