import React, { useState } from "react";
import Header from "./Header";
import Footer from "./Footer";
import Logement from "../Simulateur/Logement";
import Information from "../Simulateur/Information";
import Credit from "../Simulateur/Credit";
import Trajet from "../Simulateur/Trajet";
import Resultat from "../Simulateur/Resultat";
import { Provider as FieldsProvider } from "../../context/Fields";

const Layout: React.FC = (props) => {
  const [nbCredits, setNbCredits] = useState([0]);

  const handleClicCredit = () => {
    setNbCredits((nbCredits) => [...nbCredits, nbCredits.length]);
  };

  return (
    <React.Fragment>
      <FieldsProvider>
        <Header></Header>
        <Information></Information>
        <Logement></Logement>
        <button onClick={handleClicCredit}>Ajouter Crédit</button>
        <div>
          {nbCredits.map((id) => {
            return <Credit key={id} idCredit={id}></Credit>;
          })}
        </div>
        <Trajet></Trajet>
        <Resultat></Resultat>
        {props.children}
        <Footer></Footer>
      </FieldsProvider>
    </React.Fragment>
  );
};

export default Layout;
