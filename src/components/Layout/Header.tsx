import React from "react";

export const Header: React.FC = () => {

  return (
    <header>
      <h1>Simulateur d'emprunt immobilier</h1>
    </header>
  );
};

export default Header;
