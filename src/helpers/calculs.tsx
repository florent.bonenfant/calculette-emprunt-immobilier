import { Decimal } from "decimal.js";

export function castFloat(number: any) {
  number = parseFloat(number);
  if (isNaN(number)) {
    number = 0;
  }
  return number;
}

/**
 * Calcul la capacité théorique de remboursement par apport au taux d'endettement à 33%
 * @param fields    Tableau de valeurs lié au contexte Fields
 */
export function calculCapaciteRemboursement(fields: any) {
  let taux = 33,
    revenue = castFloat(fields["revenue"]);
  return revenue * (taux / 100);
}

/**
 * Récupération de la consommation du véhicule par km
 * @param fields    Tableau de valeurs lié au contexte Fields
 */
function calculConsommationCarburant(fields: any) {
  let trajet_conso = castFloat(fields["trajet_conso"]);
  return trajet_conso / 100;
}

/**
 * Calcul le cout du carburant pour le trajet aller/retour au travail
 * @param fields    Tableau de valeurs lié au contexte Fields
 */
function calculCoutCarburant(fields: any) {
  let trajet_distance = castFloat(fields["trajet_distance"]),
    trajet_nb_trajet = castFloat(fields["trajet_nb_trajet"]),
    trajet_essence = castFloat(fields["trajet_essence"]);
  return (
    trajet_distance *
    trajet_nb_trajet *
    calculConsommationCarburant(fields) *
    trajet_essence
  );
}

/**
 * Cout du carburant pour le mois
 * @param fields    Tableau de valeurs lié au contexte Fields
 */
export function calculCoutCarburantTrajetParMois(fields: any) {
  // 5 jours * 4 semaines
  return calculCoutCarburant(fields) * 5 * 4;
}

/**
 * Calcul du cout du notaire pour le montant du bien indiqué
 * @param fields    Tableau de valeurs lié au contexte Fields
 */
export function calculCoutNotaire(fields: any) {
  let logement_prix = castFloat(fields["logement_prix"]),
    logement_notaire = castFloat(fields["logement_notaire"]);
  return (logement_prix * logement_notaire) / 100;
}

/**
 * Calcul le cout de l'achat (logement + notaire + agence)
 * @param fields    Tableau de valeurs lié au contexte Fields
 */
export function calculCoutAchatNotaireEtAgence(fields: any) {
  let logement_prix = castFloat(fields["logement_prix"]);
  return (
    logement_prix + calculCoutNotaire(fields) + calculCoutAgenceImmo(fields)
  );
}

/**
 * Calcul le montant à payé à l'agence immboliere (aux alentours de 5%)
 * @param fields    Tableau de valeurs lié au contexte Fields
 */
export function calculCoutAgenceImmo(fields: any) {
  let logement_prix = castFloat(fields["logement_prix"]),
    logement_agence = castFloat(fields["logement_agence"]);
  return (logement_prix * logement_agence) / 100;
}

/**
 * Prix d'achat du bien avec les interets du credit
 * @param fields    Tableau de valeurs lié au contexte Fields
 */
export function coutAchatTotal(fields: any) {
  return (
    calculCoutAchatNotaireEtAgence(fields) + calculMontantInteretsTotal(fields)
  );
}

/**
 * Calcul le montant des factures prévisionnelles indirectement liées à l'achat
 * @param fields    Tableau de valeurs lié au contexte Fields
 */
export function montantFacturesPrevisionnelles(fields: any) {
  let logement_electricite = castFloat(fields["logement_electricite"]),
    logement_assurance = castFloat(fields["logement_assurance"]),
    logement_autre = castFloat(fields["logement_autre"]),
    logement_eau = castFloat(fields["logement_eau"]);
  return (
    logement_electricite +
    logement_autre +
    logement_eau +
    logement_assurance / 12
  );
}
/**
 * Cout mensuel comprenant l'assurance du logement, les taxes, charges, carburant, factures courantes et le cout de l'emprunt mensuel
 * @param fields    Tableau de valeurs lié au contexte Fields
 */
export function coutMensuelComplet(fields: any) {
  let logement_foncier = castFloat(fields["logement_foncier"]),
    logement_charges = castFloat(fields["logement_charges"]);
  return (
    calculMontantEmpruntMensuelTotal(fields) +
    calculCoutCarburantTrajetParMois(fields) +
    montantFacturesPrevisionnelles(fields) +
    (logement_foncier + logement_charges) / 12
  );
}

/**
 * Calcul le taux mensuel de l'emprunt, retour en valeur brut (multiplié par 100 pour avoir un %)
 * @param fields    Tableau de valeurs lié au contexte Fields
 */
export function calculTauxEmpruntMensuel(credit_taux: any) {
  return new Decimal(castFloat(credit_taux))
    .dividedBy(100) // Conversion du %
    .add(1)
    .pow(1 / 12)
    .minus(1);
}

/**
 * Calcul l'emprunt mensuel total des crédits
 * @param fields    Tableau de valeurs lié au contexte Fields
 */
export function calculMontantEmpruntMensuelTotal(fields: any) {
  var montantMensuel = 0;
  Object.entries(fields).forEach(([champ, valeur]) => {
    if (champ.substring(0, 12) === "credit_taux_") {
      var id = champ.substring(champ.lastIndexOf("_") + 1);
      if (
        fields["credit_duree_" + id] &&
        fields["credit_montant_" + id] &&
        fields["credit_taux_" + id]
      ) {
        montantMensuel = montantMensuel + calculMontantEmpruntMensuel(fields, id);
      }
    }
  });
  return montantMensuel;
}

/**
 * Calcul le montant mensuel de l'emprunt à remboursé
 * @param fields    Tableau de valeurs lié au contexte Fields
 * @param id        Id du crédit
 */
export function calculMontantEmpruntMensuel(fields: any, id: any) {
  let credit_duree = castFloat(fields["credit_duree_" + id]) * 12;
  let logement_prix = castFloat(fields["credit_montant_" + id]),
    tauxMensuel = calculTauxEmpruntMensuel(fields["credit_taux_" + id]).add(
      calculTauxEmpruntMensuel(fields["credit_assurance_" + id])
    );

  var up = new Decimal(tauxMensuel)
    .add(1)
    .pow(credit_duree)
    .mul(logement_prix)
    .mul(tauxMensuel);
  var down = new Decimal(tauxMensuel).add(1).pow(credit_duree).minus(1);

  return castFloat(up.dividedBy(down).toString());
}

/**
 * Calcul le cout total des interets des crédits
 * @param fields    Tableau de valeurs lié au contexte Fields
 */
export function calculMontantInteretsTotal(fields: any) {
  var montantInterets = 0;
  Object.entries(fields).forEach(([champ, valeur]) => {
    if (champ.substring(0, 12) === "credit_taux_") {
      var id = champ.substring(champ.lastIndexOf("_") + 1);
      if (fields["credit_duree_" + id] && fields["credit_montant_" + id]) {
        montantInterets = montantInterets + calculMontantInterets(fields, id);
      }
    }
  });
  return montantInterets;
}
/**
 * Calcul le cout des interets du crédit
 * @param fields    Tableau de valeurs lié au contexte Fields
 */
export function calculMontantInterets(fields: any, id: any) {
  let credit_duree = castFloat(fields["credit_duree_" + id]),
    credit_montant = castFloat(fields["credit_montant_" + id]);
  return castFloat(
    new Decimal(calculMontantEmpruntMensuel(fields, id))
      .mul(credit_duree)
      .mul(12)
      .minus(credit_montant)
      .toString()
  );
}
/**
 * Calcul le montant des crédits
 * @param fields    Tableau de valeurs lié au contexte Fields
 */
export function calculMontantCreditTotal(fields: any) {
  var montantCredits = 0;
  Object.entries(fields).forEach(([champ, valeur]) => {
    if (champ.substring(0, 15) === "credit_montant_") {
      let id = champ.substring(champ.lastIndexOf("_") + 1);
      montantCredits = montantCredits + castFloat(fields["credit_montant_" + id]);
    }
  });
  return montantCredits;
}
